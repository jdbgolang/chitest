package main

/*
 * I think I'm kind of misusing chi here.  chi is meant as a composable router
 * for REST services, and I'm just making a website.  I'm still new enough to
 * go that I'm using it as my first-ever go web <anything> and I wanted to
 * learn about POSTing a form and using templates etc.
 *
 * So this app works, but I probably could have just used net/http instead of
 * chi for this.
 *
 */

import (
	"database/sql"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	log "github.com/inconshreveable/log15"
	_ "github.com/mattn/go-sqlite3"
)

// Globals
var config_file string = "config.toml"
var config Config
var alog MyLogger
var elog MyLogger
var db *sql.DB
var templates Templates
var r *chi.Mux

func init() { // {{{
	var err error
	config, err = NewConfig(config_file)
	if err != nil {
		// We're in init() -- we haven't got elog opened yet, so just log to
		// STDERR and bail.
		log.Crit(err.Error())
		os.Exit(1)
	}
} // }}}
func connect_db() *sql.DB { // {{{
	db, err := sql.Open("sqlite3", "var/chitest.sqlite")
	if err != nil {
		panic("Cannot connect to the database!")
	}
	return db
} // }}}
func route_css(urlpath string, tmplname string) { // {{{
	if _, ok := templates[tmplname]; !ok {
		log.Crit(fmt.Sprintf("%s: no such template.", tmplname))
		return
	}

	data := &GenericPage{}
	r.Get(urlpath, func(w http.ResponseWriter, req *http.Request) {
		start := time.Now()
		templates[tmplname].execute(data, req, w, start, time.Now())
		alog.LogReq(req, 200)
	})
} // }}}
func route_index(urlpath string, tmplname string) { // {{{
	if _, ok := templates[tmplname]; !ok {
		log.Crit(fmt.Sprintf("%s: no such template.", tmplname))
		return
	}

	data := &GenericPage{title: "Index"}
	r.Get(urlpath, func(w http.ResponseWriter, req *http.Request) {
		start := time.Now()
		templates[tmplname].execute(data, req, w, start, time.Now())
		alog.LogReq(req, 200)
	})
} // }}}
func route_name_form(urlpath string, tmplname string) { // {{{
	if _, ok := templates[tmplname]; !ok {
		log.Crit(fmt.Sprintf("%s: no such template.", tmplname))
		return
	}

	data := &NameFormPage{
		GenericPage: GenericPage{title: "Name Form"},
		FormAction:  "/name_form_handler",
	}
	r.Get(urlpath, func(w http.ResponseWriter, req *http.Request) {
		start := time.Now()
		templates[tmplname].execute(data, req, w, start, time.Now())
		alog.LogReq(req, 200)
	})
} // }}}
func route_name_form_handler(urlpath string, tmplname string) { // {{{
	if _, ok := templates[tmplname]; !ok {
		log.Crit(fmt.Sprintf("%s: no such template.", tmplname))
		return
	}

	data := &NameFormHandlerPage{GenericPage{title: "Greetings"}, Person{}}
	r.Post(urlpath, func(w http.ResponseWriter, req *http.Request) {
		start := time.Now()
		req.ParseForm()
		fname := req.Form.Get("fname")
		lname := req.Form.Get("lname")
		data.Person = Person{0, fname, lname}
		templates[tmplname].execute(data, req, w, start, time.Now())
		alog.LogReq(req, 200)
	})
} // }}}
func route_people(urlpath string, tmplname string) { // {{{
	if _, ok := templates[tmplname]; !ok {
		log.Crit(fmt.Sprintf("%s: no such template.", tmplname))
		return
	}

	data := &PeoplePage{GenericPage{title: "People"}, []Person{}}
	r.Get(urlpath, func(w http.ResponseWriter, req *http.Request) {
		start := time.Now()
		data.People = FetchPeople(db)
		templates[tmplname].execute(data, req, w, start, time.Now())
		alog.LogReq(req, 200)
	})
} // }}}
func route_errors(tmplname string) { // {{{
	if _, ok := templates[tmplname]; !ok {
		log.Crit(fmt.Sprintf("%s: no such template.", tmplname))
		return
	}

	data := &ErrorPage{
		GenericPage: GenericPage{title: "Error"},
	}

	r.NotFound(func(w http.ResponseWriter, req *http.Request) {
		start := time.Now()
		code := 404
		data.Code = strconv.Itoa(code)
		data.Message = "Not Found"
		data.Description = "Nopity nope nope just click the links there bucko."
		templates[tmplname].execute(data, req, w, start, time.Now())
		elog.LogReq(req, code)
	})

	r.MethodNotAllowed(func(w http.ResponseWriter, req *http.Request) {
		start := time.Now()
		code := 405
		data.Code = strconv.Itoa(code)
		data.Message = "Method Not Allowed"
		data.Description = fmt.Sprintf("%s?  %s??? Just click the links pal.", req.Method, req.Method)
		templates[tmplname].execute(data, req, w, start, time.Now())
		elog.LogReq(req, code)
	})
} // }}}

func main() {
	alog, _ = NewLogger(config.Logs.Access)
	elog, _ = NewLogger(config.Logs.Error)
	templates = FindTemplates(config)
	templates.ParseAll()
	templates.WatchForChanges(config.Templates.UpdateInterval)
	db = connect_db()
	r = chi.NewRouter()

	route_css("/css/main", "css/main")
	route_index("/", "index")
	route_index("/index", "index")
	route_people("/people", "people")
	route_name_form("/name_form", "name_form")
	route_name_form_handler("/name_form_handler", "name_form_handler")
	route_errors("http_error")

	// log.Info is neither the access nor error logger -- it's just the
	// default, STDERR-only application-level logger.  IRL we'd redirect
	// STDERR to app.log or some such.
	log.Info("Starting server", "Port", strconv.Itoa(config.Server.Port))
	if err := http.ListenAndServe(":"+strconv.Itoa(config.Server.Port), r); err != nil {
		fmt.Println("ListenAndServe: ", err)
	}
}
