package main

import (
	"fmt"
	"net/http"
	"os"

	log "github.com/inconshreveable/log15"
)

type MyLogger struct {
	Logger log.Logger
}

func NewLogger(logfile string) (MyLogger, error) {// {{{
	l := MyLogger{Logger: log.New()}

	l.Logger.SetHandler(
		log.MultiHandler(
			log.Must.FileHandler(logfile, log.LogfmtFormat()),
			log.StreamHandler(os.Stderr, log.LogfmtFormat()),
		),
	)

	return l, nil
}// }}}
func (l *MyLogger) LogReq(r *http.Request, code int) {// {{{
	msg := fmt.Sprintf("%s %s", r.Method, r.URL)
	l.Logger.Info(
		msg,
		"Code", code,
		"Ref", r.Referer(),
		"UA", r.UserAgent(),
		"RemoteAddr", r.RemoteAddr,
	)
}// }}}
