package main

import (
	"bufio"
	"fmt"
	"os"

	"github.com/BurntSushi/toml"
)

type DevConfig struct {
	Environment string
}
type LogConfig struct {
	Access string
	Error  string
}
type ServerConfig struct {
	Port    int
	Tmpldir string
}
type TemplateConfig struct {
	Dir            string
	Ext            string
	UpdateInterval int
}
type Config struct {
	Dev       DevConfig
	Logs      LogConfig
	Server    ServerConfig
	Templates TemplateConfig
}

func NewConfig(path string) (Config, error) { // {{{
	file, err := os.Open(path)
	if err != nil {
		return Config{}, fmt.Errorf("Could not open config file: %s", err)
	}
	defer file.Close()

	var config_text string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		// scanner.Scan() chomps the newlines, but toml.Decode() expects
		// newlines to exist between config entries.  So put the newlines
		// back.
		config_text = config_text + "\n" + scanner.Text()
	}
	if scanner.Err() != nil {
		return Config{}, fmt.Errorf("Unable to read config file: %s\n", err)
	}

	var config Config
	if _, err := toml.Decode(config_text, &config); err != nil {
		return Config{}, fmt.Errorf("Unable to decode config file: %s\n", err)
	}

	// Strip trailing slash on Dir if the user typed it
	dir_strlen := len(config.Templates.Dir)
	lastchar := string(config.Templates.Dir[dir_strlen-1])
	if lastchar == "/" {
		config.Templates.Dir = config.Templates.Dir[0 : dir_strlen-1]
	}

	return config, nil
} // }}}
