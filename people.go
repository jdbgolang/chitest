package main

import (
	"database/sql"
)

type Person struct {
	ID    int
	Fname string
	Lname string
}

func FetchPeople(dbh *sql.DB) []Person {// {{{
	rows, err := dbh.Query("SELECT * FROM People")
	if err != nil {
		panic("Sending people query: " + err.Error())
	}
	defer rows.Close()

	ppl := make([]Person, 0)
	for rows.Next() {
		var id int
		var fname, lname string
		err = rows.Scan(&id, &fname, &lname)
		if err != nil {
			panic("Scanning people query: " + err.Error())
		}

		ppl = append(ppl, Person{id, fname, lname})
	}
	return ppl
}// }}}
