package main

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"time"

	log "github.com/inconshreveable/log15"
)

// Individual html/template
type Template struct {
	Key   string             // the key IDing this template in the Templates map
	Path  string             // full system path
	Mtime time.Time          // lastmodtime
	Tmpl  *template.Template // template object parsed from file in Path
}

// Collection of Template structs.
// Map: path, relative to Config.Templates.Dir => Template
// eg "index.html" => Template Struct
type Templates map[string]*Template

type WebPage interface {
	Title() string
	SetTitle(string)
	IP() string
	SetIP(string)
	PageFooter() template.HTML // string with trusted HTML that does not get escaped.
	SetPageFooter(string)
}

// All pages inherit from GenericPage
type GenericPage struct {
	title  string
	ip     string
	footer string
}

func (p *GenericPage) Title() string {
	return p.title
}
func (p *GenericPage) SetTitle(t string) {
	p.title = t
}
func (p *GenericPage) IP() string {
	return p.ip
}
func (p *GenericPage) SetIP(ip string) {
	p.ip = ip
}
func (p *GenericPage) SetPageFooter(s string) {
	p.footer = s
}
func (p *GenericPage) PageFooter() template.HTML {
	return template.HTML(p.footer)
}

// HTTP error (404 etc)
type ErrorPage struct {
	GenericPage
	Code        string
	Message     string
	Description string
}
type NameFormHandlerPage struct {
	GenericPage
	Person
}
type NameFormPage struct {
	GenericPage
	FormAction string
}
type PeoplePage struct {
	GenericPage
	People []Person
}

/* Template Methods */

// Parse the current template from its Path and store the parsed template
// object in the Tmpl key.  Meant to be called as a goroutine.
func (t *Template) GoParse(wg *sync.WaitGroup, c chan<- error) {
	err := t.Parse()
	c <- err
	wg.Done()
}

// Parse the current template from its Path and store the parsed template
// object in the Tmpl key.  Meant to be called synchronously.
// If a template fails to parse, this returns an error and does not replace
// the previous parsed template.
func (t *Template) Parse() error { // {{{
	parsed_template, err := template.ParseFiles(t.Path)
	if err != nil {
		return err
	}
	t.Tmpl = parsed_template
	return nil
} // }}}

// Return true if the underlying file has been updated since it was most
// recently parsed (ie a user edited a template file).
func (t *Template) IsUpdated() (bool, error) { // {{{
	finfo, err := os.Stat(t.Path)
	if err != nil {
		return false, err
	}

	if finfo.ModTime().Sub(t.Mtime).Seconds() > 1 {
		return true, nil
	}
	return false, nil
} // }}}

// Watch the template; if its mtime changes, re-parse it.
func (t *Template) WatchForChanges(seconds int, coll *Templates) {
	for {
		updated, err := t.IsUpdated()
		if err != nil {
			log.Info(fmt.Sprintf("Template file %s has been removed!", t.Path))
		}
		if updated {
			err := t.Parse()
			if err != nil {
				log.Warn(fmt.Sprintf("Parse failed: %s", err.Error()))
			}
		}
		dur := time.Duration(seconds) * NS_TO_SEC
		time.Sleep(dur)
	}
}

// Return the difference between two times as an int and a label, eg (100,
// "ns") for "100 nanoseconds" or (334, μs) for "334 microseconds" etc.  Label
// can be from "ns" to "seconds".
func (t *Template) time_diff(s time.Time, e time.Time) (int64, string) { // {{{
	el := e.Sub(s)
	n := el.Nanoseconds()

	var label string = "ns"
	var elapsed int64

	switch {
	case n < 1000:
		elapsed = n
	case n > 1000, n < 1000000:
		label = "μs"
		elapsed = n / 1000
	case n > 1000000, n < 1000000000:
		label = "ms"
		elapsed = n / 1000000
	default:
		label = "seconds"
		elapsed = n / 1000000000
	}
	return elapsed, label
} // }}}

// Check if template file exists
func (t *Template) exists() bool { // {{{
	fname := fmt.Sprintf("%s/%s", config.Templates.Dir, t.Path)
	if _, err := os.Stat(fname); os.IsExist(err) {
		return true
	}
	return false
} // }}}

// Returns debug footer as HTML string.
func (t *Template) debug_footer(s time.Time, e time.Time) string { // {{{
	var footer string
	elapsed, label := t.time_diff(s, e)
	footer = fmt.Sprintf("Page generated in %d %s.", elapsed, label)
	return fmt.Sprintf("<div class='debug_footer'>%s</span>", footer)
} // }}}

// Sets data that should be available to all templates and executes the
// template.
//
// In the dev environment, this executes the template to a dummy buffer and
// times that execution, then re-executes it to the ResponseWriter which sends
// output to the browser.  This way the "page generated" time displayed at the
// bottom of the page does include the time needed to parse the template
// itself.
// It's important to keep in mind that the reported time includes the time of
// writing to the dummy buffer, NOT the actual time take in producing the
// template to our IO writer -- that's a kind of chicken-and-egg issue.
// The result is that the reported time should be pretty close to accurate,
// but won't ever be exact.
func (t *Template) execute(data WebPage, req *http.Request, w http.ResponseWriter, start time.Time, end time.Time) { // {{{
	data.SetIP(req.RemoteAddr)

	if config.Dev.Environment == "dev" {
		var tpl bytes.Buffer
		t.Tmpl.Execute(&tpl, data)
		data.SetPageFooter(t.debug_footer(start, time.Now()))
		t.Tmpl.Execute(w, data)
	} else {
		t.Tmpl.Execute(w, data)
	}
} // }}}

/* Templates Methods */

// Parse all templates, defined as files with the template extension and
// located in the templates directory, both as specified in the conf file.
// Intended to be called only once in main() on server startup, so if any
// templates fail to parse, this will list out all such failures and exit.
func (t *Templates) ParseAll() { // {{{
	var wg sync.WaitGroup
	var chans []chan error
	for _, tmpl := range *t {
		wg.Add(1)
		c := make(chan error)
		chans = append(chans, c)

		go tmpl.GoParse(&wg, c)
	}

	var bad_tmpl bool = false
	for _, parsechan := range chans {
		err := <-parsechan
		if err != nil {
			fmt.Printf("Parse: %s\n", err.Error())
			bad_tmpl = true
		}
	}
	if bad_tmpl {
		os.Exit(1)
	}

	wg.Wait()
} // }}}

// Watch all templates and re-parse any if their mtime changes.
func (templates *Templates) WatchForChanges(sec int) { // {{{
	for _, t := range *templates {
		go t.WatchForChanges(sec, templates)
	}
} // }}}

/* Static functions */

// Iterate all template files, defined as having the right extension and
// located anywhere under the templates directory.  The "right extension" and
// "templates directory" are both specified in the config file.  Return a
// Templates map.
func FindTemplates(conf Config) Templates {
	nt := make(Templates)
	path_striplen := len(conf.Templates.Dir) + 1 // Dir has no trailing slash; add 1 to include it.
	ext_striplen := len(conf.Templates.Ext)

	err := filepath.Walk(conf.Templates.Dir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if filepath.Ext(path) == conf.Templates.Ext {
				// strip "tmpl/" from front of path
				tmpl_key := path[path_striplen:]

				// strip ".ext" from end of path
				this_ext_striplen := len(tmpl_key) - ext_striplen
				tmpl_key = tmpl_key[0:this_ext_striplen]

				nt[tmpl_key] = &Template{Key: tmpl_key, Path: path, Mtime: info.ModTime()}
			}
			return nil
		},
	)
	if err != nil {
		panic(err)
	}

	return nt
}
