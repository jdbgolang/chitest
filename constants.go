package main

// time.Duration is in nanoseconds.
// 		eg: seconds := time.Duration(sec) * NS_TO_SEC
const NS_TO_μS = 1000             // nano to micro; μs := time.Duration(3) * NS_TO_SEC
const NS_TO_MS = NS_TO_μS * 1000  // nano to milli; ms := time.Duration(3) * NS_TO_SEC
const NS_TO_SEC = NS_TO_MS * 1000 // nano to second; seconds := time.Duration(3) * NS_TO_SEC
